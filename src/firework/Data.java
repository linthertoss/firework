package firework;

public class Data {
	public float x;
	public float y;

	public Data(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public void add(Data data) {
		this.x += data.x;
		this.y += data.y;

	}
	
	public void multi(float data) {
		this.x *= data;
		this.y *= data;
	}

}
