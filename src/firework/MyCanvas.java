package firework;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
import java.util.Vector;

public class MyCanvas extends Canvas implements Runnable {
	private static final long serialVersionUID = 1L;

	public static boolean isStarted;
	public static Vector<Firework> fireworks;
	private Random rd = new Random();

	public MyCanvas() {
		fireworks = new Vector<Firework>();
		fireworks.addElement(new Firework());
	}

	@Override
	public void run() {
		while (isStarted) {
			float a = rd.nextFloat();
			//System.out.println(a);
			if (a < 0.05) {
				fireworks.addElement(new Firework());
//				System.out.println(fireworks.size());
			}
			this.repaint();
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
			}
		}

	}

	public void started() {
		if (!isStarted) {
			new Thread(this).start();
			isStarted = true;
		}
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(0, 0, 400, 400);
		for (int i = 0; i < fireworks.size(); i++) {
			Firework firework = fireworks.elementAt(i);
			System.out.println("potision "+ i+ "----   " + firework.firework.potision.y);
			firework.update();
			firework.draw(g);
			
		}
		if(fireworks.size() > 15) {
			for (int i = 0; i < 5; i++) {
				fireworks.removeElementAt(i);
				
			}
		}
//		System.out.println(fireworks.size());
	}

}
