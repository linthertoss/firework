package firework;

import java.awt.Graphics;

public interface ExplodeStyle {
	
	public void update();
	public void addGravity(Data data);
	public void draw(Graphics g);

}
