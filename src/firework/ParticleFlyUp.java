package firework;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class ParticleFlyUp implements ExplodeStyle {

	public Data potision;// toa do
	public Data velocity;// van toc
	public Data accelerator; // tăng tốc

	private Random rd = new Random();

	public ParticleFlyUp(float x, float y) {
		potision = new Data(x, y);
		velocity = new Data(0, rd.nextInt(4) - 11);
		accelerator = new Data(0, 0);
	}

	@Override
	public void update() {
		potision.add(velocity);
		velocity.add(accelerator);
		accelerator.multi(0f);
	}

	@Override
	public void addGravity(Data data) {
		this.accelerator.add(data);

	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.blue);
//		System.out.println(this.potision.x+" "+this.potision.y);
		g.fillRect((int)this.potision.x, (int)this.potision.y, 2, 2);

	}

}
