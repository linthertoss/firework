package firework;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class CricleStyle implements ExplodeStyle {
	public Data potision;// toa do
	public Data velocity;// van toc
	public Data accelerator; // tăng tốc

	private Random rd = new Random();
	
	public CricleStyle(float x, float y) {
		this.potision = new Data(x, y);
		int radius = rd.nextInt(360);
		double tempX = Math.cos(radius);
		double tempY = Math.sin(radius);
		velocity = new Data((float)tempX, (float)tempY);
		velocity.multi(this.rd.nextInt(4)+4);
		accelerator = new Data(0, 0);
				
	}
	
	@Override
	public void update() {
		velocity.multi(0.92f);
		potision.add(velocity);
		velocity.add(accelerator);
		accelerator.multi(0f);

	}

	@Override
	public void addGravity(Data data) {
		accelerator.add(data);

	}

	@Override
	public void draw(Graphics g) {
		float r = rd.nextFloat();
		float gg = rd.nextFloat();
		float b = rd.nextFloat();
		g.setColor(new Color(r, gg, b));
		g.fillRect((int)this.potision.x, (int)this.potision.y, 2, 2);
	}

}
