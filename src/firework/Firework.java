package firework;

import java.awt.Graphics;
import java.util.Random;
import java.util.Vector;

public class Firework {

	public ParticleFlyUp firework;
	private Random rd = new Random();
	public Vector<CricleStyle> particleBoom;
	public Data gravity; // trọng lực

	public boolean isBoom;

	public Firework() {
		firework = new ParticleFlyUp(rd.nextInt(400), 400);
		gravity = new Data(0, 0.2f);
		particleBoom = new Vector<CricleStyle>();
		isBoom = false;
	}

	public void update() {
		if (!isBoom) {
			this.firework.addGravity(gravity);
			this.firework.update();
			if (this.firework.velocity.y >= 0) {
				isBoom = true;
				this.init();
			}
		}else {
			for (int i = 0; i < this.particleBoom.size(); i++) {
				CricleStyle cricleStyle = particleBoom.elementAt(i);
				cricleStyle.addGravity(gravity);
				cricleStyle.update();
				
			}
		}

	}

	private void init() {
		for (int i = 0; i < 69; i++) {
			CricleStyle cricleStyle = new CricleStyle(this.firework.potision.x, this.firework.potision.y);
			this.particleBoom.addElement(cricleStyle);
		}

	}

	public void draw(Graphics g) {
		if (!isBoom) {
			this.firework.draw(g);
		} else {
			for (int i = 0; i < this.particleBoom.size(); i++) {
				CricleStyle cricleStyle = particleBoom.elementAt(i);
				cricleStyle.update();
				cricleStyle.draw(g);
			}
		}
	}
}
