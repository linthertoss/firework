package firework;

import javax.swing.JFrame;

public class App extends JFrame {
	public static void main(String[] args) {
		MyCanvas canvas = new MyCanvas();
		App app = new App();
		app.init();
		app.add(canvas);
		canvas.started();
	}
	
	public void init() {
		this.setVisible(true);
		this.setSize(400,400);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
